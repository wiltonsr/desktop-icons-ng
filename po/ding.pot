# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ding package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ding\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-23 22:38+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: app/askRenamePopup.js:46
msgid "Folder name"
msgstr ""

#: app/askRenamePopup.js:46
msgid "File name"
msgstr ""

#: app/askRenamePopup.js:54 app/autoAr.js:305 app/desktopManager.js:977
msgid "OK"
msgstr ""

#: app/askRenamePopup.js:54
msgid "Rename"
msgstr ""

#: app/autoAr.js:88
msgid "AutoAr is not installed"
msgstr ""

#: app/autoAr.js:89
msgid ""
"To be able to work with compressed files, install file-roller and/or gir-1.2-"
"gnomeAutoAr"
msgstr ""

#: app/autoAr.js:224
msgid "Extracting files"
msgstr ""

#: app/autoAr.js:241
msgid "Compressing files"
msgstr ""

#: app/autoAr.js:297 app/autoAr.js:636 app/desktopManager.js:979
#: app/fileItemMenu.js:460
msgid "Cancel"
msgstr ""

#: app/autoAr.js:318 app/autoAr.js:619
msgid "Enter a password here"
msgstr ""

#: app/autoAr.js:359
msgid "Removing partial file '${outputFile}'"
msgstr ""

#: app/autoAr.js:378
msgid "Creating destination folder"
msgstr ""

#: app/autoAr.js:410
msgid "Extracting files into '${outputPath}'"
msgstr ""

#: app/autoAr.js:442
msgid "Extraction completed"
msgstr ""

#: app/autoAr.js:443
msgid "Extracting '${fullPathFile}' has been completed."
msgstr ""

#: app/autoAr.js:449
msgid "Extraction cancelled"
msgstr ""

#: app/autoAr.js:450
msgid "Extracting '${fullPathFile}' has been cancelled by the user."
msgstr ""

#: app/autoAr.js:460
msgid "Passphrase required for ${filename}"
msgstr ""

#: app/autoAr.js:463
msgid "Error during extraction"
msgstr ""

#: app/autoAr.js:492
msgid "Compressing files into '${outputFile}'"
msgstr ""

#: app/autoAr.js:505
msgid "Compression completed"
msgstr ""

#: app/autoAr.js:506
msgid "Compressing files into '${outputFile}' has been completed."
msgstr ""

#: app/autoAr.js:510 app/autoAr.js:517
msgid "Cancelled compression"
msgstr ""

#: app/autoAr.js:511
msgid "The output file '${outputFile}' already exists."
msgstr ""

#: app/autoAr.js:518
msgid "Compressing files into '${outputFile}' has been cancelled by the user."
msgstr ""

#: app/autoAr.js:521
msgid "Error during compression"
msgstr ""

#: app/autoAr.js:554
msgid "Create archive"
msgstr ""

#: app/autoAr.js:579
msgid "Archive name"
msgstr ""

#: app/autoAr.js:614
msgid "Password"
msgstr ""

#: app/autoAr.js:633
msgid "Create"
msgstr ""

#: app/autoAr.js:708
msgid "Compatible with all operating systems."
msgstr ""

#: app/autoAr.js:714
msgid "Password protected .zip, must be installed on Windows and Mac."
msgstr ""

#: app/autoAr.js:720
msgid "Smaller archives but Linux and Mac only."
msgstr ""

#: app/autoAr.js:726
msgid "Smaller archives but must be installed on Windows and Mac."
msgstr ""

#: app/dbusUtils.js:68
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr ""

#: app/dbusUtils.js:69
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""

#: app/desktopIconsUtil.js:136
msgid "Command not found"
msgstr ""

#: app/desktopManager.js:257
msgid "Nautilus File Manager not found"
msgstr ""

#: app/desktopManager.js:258
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""

#: app/desktopManager.js:939
msgid "Clear Current Selection before New Search"
msgstr ""

#: app/desktopManager.js:981
msgid "Find Files on Desktop"
msgstr ""

#: app/desktopManager.js:1047 app/desktopManager.js:1750
msgid "New Folder"
msgstr ""

#: app/desktopManager.js:1051
msgid "New Document"
msgstr ""

#: app/desktopManager.js:1056
msgid "Paste"
msgstr ""

#: app/desktopManager.js:1060
msgid "Undo"
msgstr ""

#: app/desktopManager.js:1064
msgid "Redo"
msgstr ""

#: app/desktopManager.js:1070
msgid "Select All"
msgstr ""

#: app/desktopManager.js:1078
msgid "Show Desktop in Files"
msgstr ""

#: app/desktopManager.js:1082 app/fileItemMenu.js:358
msgid "Open in Terminal"
msgstr ""

#: app/desktopManager.js:1088
msgid "Change Background…"
msgstr ""

#: app/desktopManager.js:1099
msgid "Desktop Icons Settings"
msgstr ""

#: app/desktopManager.js:1103
msgid "Display Settings"
msgstr ""

#: app/desktopManager.js:1763
msgid "Folder Creation Failed"
msgstr ""

#: app/desktopManager.js:1764
msgid "Error while trying to create a Folder"
msgstr ""

#: app/desktopManager.js:1800
msgid "Template Creation Failed"
msgstr ""

#: app/desktopManager.js:1801
msgid "Error while trying to create a Document"
msgstr ""

#: app/desktopManager.js:1809
msgid "Arrange Icons"
msgstr ""

#: app/desktopManager.js:1813
msgid "Arrange By..."
msgstr ""

#: app/desktopManager.js:1822
msgid "Keep Arranged..."
msgstr ""

#: app/desktopManager.js:1826
msgid "Keep Stacked by type..."
msgstr ""

#: app/desktopManager.js:1831
msgid "Sort Home/Drives/Trash..."
msgstr ""

#: app/desktopManager.js:1837
msgid "Sort by Name"
msgstr ""

#: app/desktopManager.js:1839
msgid "Sort by Name Descending"
msgstr ""

#: app/desktopManager.js:1842
msgid "Sort by Modified Time"
msgstr ""

#: app/desktopManager.js:1845
msgid "Sort by Type"
msgstr ""

#: app/desktopManager.js:1848
msgid "Sort by Size"
msgstr ""

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: app/fileItem.js:168
msgid "Home"
msgstr ""

#: app/fileItem.js:291
msgid "Broken Link"
msgstr ""

#: app/fileItem.js:292
msgid "Can not open this File because it is a Broken Symlink"
msgstr ""

#: app/fileItem.js:346
msgid "Broken Desktop File"
msgstr ""

#: app/fileItem.js:347
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"\t<b>Edit the file to set the correct executable Program.</b>"
msgstr ""

#: app/fileItem.js:353
msgid "Invalid Permissions on Desktop File"
msgstr ""

#: app/fileItem.js:354
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""

#: app/fileItem.js:356
msgid ""
"\n"
"<b>Set Permissions, in \"Others Access\", \"Read Only\" or \"None\"</b>"
msgstr ""

#: app/fileItem.js:359
msgid ""
"\n"
"<b>Enable option, \"Allow Executing File as a Program\"</b>"
msgstr ""

#: app/fileItem.js:367
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"<b>Enable \"Allow Launching\"</b>"
msgstr ""

#: app/fileItemMenu.js:135
msgid "Open All..."
msgstr ""

#: app/fileItemMenu.js:135
msgid "Open"
msgstr ""

#: app/fileItemMenu.js:151
msgid "Stack This Type"
msgstr ""

#: app/fileItemMenu.js:151
msgid "Unstack This Type"
msgstr ""

#: app/fileItemMenu.js:164
msgid "Scripts"
msgstr ""

#: app/fileItemMenu.js:170
msgid "Open All With Other Application..."
msgstr ""

#: app/fileItemMenu.js:170
msgid "Open With Other Application"
msgstr ""

#: app/fileItemMenu.js:176
msgid "Launch using Dedicated Graphics Card"
msgstr ""

#: app/fileItemMenu.js:188
msgid "Run as a program"
msgstr ""

#: app/fileItemMenu.js:196
msgid "Cut"
msgstr ""

#: app/fileItemMenu.js:203
msgid "Copy"
msgstr ""

#: app/fileItemMenu.js:211
msgid "Rename…"
msgstr ""

#: app/fileItemMenu.js:221
msgid "Move to Trash"
msgstr ""

#: app/fileItemMenu.js:229
msgid "Delete permanently"
msgstr ""

#: app/fileItemMenu.js:239
msgid "Don't Allow Launching"
msgstr ""

#: app/fileItemMenu.js:239
msgid "Allow Launching"
msgstr ""

#: app/fileItemMenu.js:252
msgid "Empty Trash"
msgstr ""

#: app/fileItemMenu.js:265
msgid "Eject"
msgstr ""

#: app/fileItemMenu.js:273
msgid "Unmount"
msgstr ""

#: app/fileItemMenu.js:287 app/fileItemMenu.js:294
msgid "Extract Here"
msgstr ""

#: app/fileItemMenu.js:301
msgid "Extract To..."
msgstr ""

#: app/fileItemMenu.js:310
msgid "Send to..."
msgstr ""

#: app/fileItemMenu.js:318
msgid "Compress {0} folder"
msgid_plural "Compress {0} folders"
msgstr[0] ""
msgstr[1] ""

#: app/fileItemMenu.js:325
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] ""
msgstr[1] ""

#: app/fileItemMenu.js:333
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] ""
msgstr[1] ""

#: app/fileItemMenu.js:344
msgid "Common Properties"
msgstr ""

#: app/fileItemMenu.js:344
msgid "Properties"
msgstr ""

#: app/fileItemMenu.js:351
msgid "Show All in Files"
msgstr ""

#: app/fileItemMenu.js:351
msgid "Show in Files"
msgstr ""

#: app/fileItemMenu.js:435
msgid "No Extraction Folder"
msgstr ""

#: app/fileItemMenu.js:436
msgid "Unable to extract File, extraction Folder Does not Exist"
msgstr ""

#: app/fileItemMenu.js:456
msgid "Select Extract Destination"
msgstr ""

#: app/fileItemMenu.js:461
msgid "Select"
msgstr ""

#: app/fileItemMenu.js:502
msgid "Can not email a Directory"
msgstr ""

#: app/fileItemMenu.js:503
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr ""

#: app/notifyX11UnderWayland.js:37
msgid "Desktop Icons NG is running under X11Wayland"
msgstr ""

#: app/notifyX11UnderWayland.js:38
msgid ""
"It seems that you have your system configured to force GTK to use X11. This "
"works, but it's suboptimal. You should check your system configuration to "
"fix this."
msgstr ""

#: app/notifyX11UnderWayland.js:39 app/showErrorPopup.js:39
msgid "Close"
msgstr ""

#: app/notifyX11UnderWayland.js:47
msgid "Don't show this message anymore."
msgstr ""

#: app/preferences.js:91
msgid "Settings"
msgstr ""

#: app/prefswindow.js:64
msgid "Size for the desktop icons"
msgstr ""

#: app/prefswindow.js:64
msgid "Tiny"
msgstr ""

#: app/prefswindow.js:64
msgid "Small"
msgstr ""

#: app/prefswindow.js:64
msgid "Standard"
msgstr ""

#: app/prefswindow.js:64
msgid "Large"
msgstr ""

#: app/prefswindow.js:65
msgid "Show the personal folder in the desktop"
msgstr ""

#: app/prefswindow.js:66
msgid "Show the trash icon in the desktop"
msgstr ""

#: app/prefswindow.js:67 schemas/org.gnome.shell.extensions.ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr ""

#: app/prefswindow.js:68 schemas/org.gnome.shell.extensions.ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr ""

#: app/prefswindow.js:71
msgid "New icons alignment"
msgstr ""

#: app/prefswindow.js:73
msgid "Top-left corner"
msgstr ""

#: app/prefswindow.js:74
msgid "Top-right corner"
msgstr ""

#: app/prefswindow.js:75
msgid "Bottom-left corner"
msgstr ""

#: app/prefswindow.js:76
msgid "Bottom-right corner"
msgstr ""

#: app/prefswindow.js:78 schemas/org.gnome.shell.extensions.ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr ""

#: app/prefswindow.js:79
msgid "Highlight the drop place during Drag'n'Drop"
msgstr ""

#: app/prefswindow.js:80 schemas/org.gnome.shell.extensions.ding.gschema.xml:90
msgid "Use Nemo to open folders"
msgstr ""

#: app/prefswindow.js:82
msgid "Add an emblem to soft links"
msgstr ""

#: app/prefswindow.js:84
msgid "Use dark text in icon labels"
msgstr ""

#: app/prefswindow.js:91
msgid "Settings shared with Nautilus"
msgstr ""

#: app/prefswindow.js:113
msgid "Click type for open files"
msgstr ""

#: app/prefswindow.js:113
msgid "Single click"
msgstr ""

#: app/prefswindow.js:113
msgid "Double click"
msgstr ""

#: app/prefswindow.js:114
msgid "Show hidden files"
msgstr ""

#: app/prefswindow.js:115
msgid "Show a context menu item to delete permanently"
msgstr ""

#: app/prefswindow.js:120
msgid "Action to do when launching a program from the desktop"
msgstr ""

#: app/prefswindow.js:121
msgid "Display the content of the file"
msgstr ""

#: app/prefswindow.js:122
msgid "Launch the file"
msgstr ""

#: app/prefswindow.js:123
msgid "Ask what to do"
msgstr ""

#: app/prefswindow.js:129
msgid "Show image thumbnails"
msgstr ""

#: app/prefswindow.js:130
msgid "Never"
msgstr ""

#: app/prefswindow.js:131
msgid "Local files only"
msgstr ""

#: app/prefswindow.js:132
msgid "Always"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:25
msgid "Icon size"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:30
msgid "Show personal folder"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:35
msgid "Show trash icon"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:40
msgid "New icons start corner"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:51
msgid "Show mounted network volumes in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:75
msgid "Arrange Order"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:80
msgid "Keep Icons Stacked"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:81
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:91
msgid "Use Nemo instead of Nautilus to open folders."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:95
msgid "Add an emblem to links"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:96
msgid "Add an emblem to allow to identify soft links."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:100
msgid "Use black for label text"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:101
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:105
msgid "Show a popup if running on X11Wayland"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:106
msgid ""
"Whether DING should show a popup if it is running on X11Wayland, or the user "
"decided to not show it anymore."
msgstr ""
